const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.phoneNumber = req.body.phoneNumber;
    user.password = req.body.password;
    const onlyGmail = user.email.indexOf('@gmail.com') === -1;
    const phoneNumberStart = '+380';

    if(user.firstName && user.lastName && user.email && user.phoneNumber && user.password){
        if(req.body.id) {
            res.body = res.status(400).json({
                error: true,
                message: 'Id shouldn\'t be provided in body'
            });
        }
        if(user.password.length < 3) {
            res.body = res.status(400).json({
                error: true,
                message: 'Password should be more than 3 symbols'
            })
        }
        if(onlyGmail) {
            res.body = res.status(400).json({
                error: true,
                message: 'You can use google mail only'
            });
        }
        if(!user.phoneNumber.startsWith(phoneNumberStart)) {
            res.body = res.status(400).json({
                error: true,
                message: 'Phone number should start with +380'
            });
        }
        if(user.phoneNumber.length < 13) {
            res.body = res.status(400).json({
                error: true,
                message: 'Enter the full number'
            })
        }
    }else {
        res.body = res.status(400).json({
            error: true,
            message: 'User entity to create isn\'t correct'
        });
        next(res.body);
    }
    // TODO: Implement validatior for user entity during creation

    next();
}

const updateUserValid = (req, res, next) => {
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.phoneNumber = req.body.phoneNumber;
    user.password = req.body.password;
    const phoneNumberStart = '+380';
    const shouldContainOnly = ['firstName','lastName','email','phoneNumber','password'];

    for(let [key] of Object.entries(req.body)){
        if(!shouldContainOnly.includes(key)) {
            res.body = res.status(400).json({
                error: true,
                message: `Request contains unknown property ${key}`
            });
            next(res.body);
        }
    }

    if(user.firstName || user.lastName || user.email || user.phoneNumber || user.password){
        if(req.body.id) {
            res.body = res.status(400).json({
                error: true,
                message: 'Id shouldn\'t be provided in body'
            });
            next(res.body);
        }
        if(user.password && user.password.length < 3) {
            res.body = res.status(400).json({
                error: true,
                message: 'Password should be more than 3 symbols'
            });
            next(res.body);
        }
        if(user.phoneNumber && !user.phoneNumber.startsWith(phoneNumberStart)) {
            res.body = res.status(400).json({
                error: true,
                message: 'Phone number should start with +380'
            });
            next(res.body);
        }
        if(user.phoneNumber && user.phoneNumber.length < 13) {
            res.body = res.status(400).json({
                error: true,
                message: 'Enter the full number'
            });
            next(res.body);
        }
    }else {
        res.body = res.status(400).json({
            error: true,
            message: 'User entity to update isn\'t correct'
        });
        next(res.body);
    }
    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;