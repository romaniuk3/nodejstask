const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getFighters() {
        const allFighters = FighterRepository.getAll();
        if(!allFighters) {
            return null;
        }
        return allFighters;
    }
    createFighter(fighter) {
        const checkFighter = FighterRepository.getOne({name: fighter.name});
        if(checkFighter) {
            throw Error('This fighter is already exists');
        }
        return FighterRepository.create(fighter);
    }

    searchFighter(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            throw Error(`There is no fighter with this id`);
        }
        return item;
    }

    deleteFighter(id) {
        const checkFighter = FighterRepository.getOne({id});
        if(!checkFighter) {
            throw Error('There is no fighter with this id');
        }else {
            return FighterRepository.delete(id);
        } 
    }

    updateFighter(id, data) {
        const reqFighter = FighterRepository.getOne({id});
        const updateFighter = FighterRepository.update(id, data);
        if(!reqFighter) {
            throw Error(`There is no fighter with this id`);
        }else if(!updateFighter) {
            throw Error(`You can't update this fighter`);
        }
        return updateFighter;
    }
    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();