const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getUsers() {
        const allUsers = UserRepository.getAll();
        if(allUsers.length < 1) {
            throw Error('There are no people in db');
        }else {
            return allUsers;
        }
    }

    createUser(user) {
        const checkMail = UserRepository.getOne({email: user.email});
        const checkNumber = UserRepository.getOne({phoneNumber: user.phoneNumber});

        if(checkMail) {
            throw Error('User with this email is already exists');
        }else if(checkNumber) {
            throw Error('User with this number is already exists');
        }
        return UserRepository.create(user);
    }

    updateUser(id, data) {
        const reqUser = UserRepository.getOne({id});
        const updateUser = UserRepository.update(id, data);
        if(!reqUser) {
            throw Error(`There is no user with this id`);
        }else if(!updateUser) {
            throw Error(`You can't update this user`);
        }
        return updateUser;
    }

    removeUser(id) {
        const reqUser = UserRepository.getOne({id});
        if(!reqUser) {
            throw Error(`There is no user with this id`);
        }else {
            const remove = UserRepository.delete(id);
            return remove;
        }
    }
    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error(`There is no user with this id`);
        }
        return item;
    }
}

module.exports = new UserService();