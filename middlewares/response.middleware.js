const responseMiddleware = (req, res, next) => {
    if(res.data) {
        res.send(res.data);
    }else {
        res.body = res.json({
            error: true,
            message: res.err.message
        })
    }
    next();
}

exports.responseMiddleware = responseMiddleware;