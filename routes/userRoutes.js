const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req,res, next) => {
  try {
    const allUsers = UserService.getUsers();
    res.data = allUsers;
  } catch(err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req,res,next) => {
  try {
    const id = req.params.id;
    const someUser = UserService.search({id});
    res.data = someUser;
  }catch(err) {
    res.status(404)
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/',createUserValid ,(req,res,next) => {
  try {
    const data = UserService.createUser(req.body);
    console.log(data);
    res.data = data

  } catch (err) {
    res.status(400);
    res.err = err;
  }finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req,res,next) => {
  try {
    const id = req.params.id;
    const dataForUpdate = req.body;
    const updateUser = UserService.updateUser(id, dataForUpdate);
    res.data = updateUser;
  } catch(err) {
    res.status(400);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req,res,next) => {
  try {
    const id = req.params.id;
    const deleteUser = UserService.removeUser(id);
    if(deleteUser) {
      res.data = {deleted: true, message: 'User was deleted'};
    }
  } catch(err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
// TODO: Implement route controllers for user

module.exports = router;
