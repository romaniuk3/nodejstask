const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req,res,next) => {
    try {
        const allFighters = FighterService.getFighters();
        res.data = allFighters;
    } catch(err){
        res.status(404);
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req,res,next) => {
    try {
        const id = req.params.id;
        const reqFighter = FighterService.searchFighter({id})
        res.data = reqFighter;
    } catch (err) {
        res.status(404);
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.delete('/:id', (req,res,next) => {
    try{
        const id = req.params.id;
        const deleteFighter = FighterService.deleteFighter(id);
        if(deleteFighter){
            res.data = {deleted: true, message: 'Fighter was deleted'}
        }
    } catch(err) {
        res.status(404);        
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req,res,next) => {
    try{
        const id = req.params.id;
        const dataForUpdate = req.body;
        const updateFighter = FighterService.updateFighter(id, dataForUpdate);
        res.data = updateFighter;
    } catch(err) {
        res.status(404);
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req,res,next) => {
    try {
        const fighter = FighterService.createFighter(req.body);
        res.data = fighter;
    } catch(err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
// TODO: Implement route controllers for fighter

module.exports = router;