const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    fighter.defense = req.body.defense;
    fighter.name = req.body.name;
    fighter.power = req.body.power;

    if(fighter.name && fighter.power && fighter.defense) {
        if(req.body.id) {
            res.body = res.status(400).json({
                error: true,
                message: 'Id shouldn\'t be provided in body'
            });
            next(res.body);
        }
        if(req.body.health && req.body.health <= 120 && req.body.health >= 80) {
            fighter.health = req.body.health;
        }else {
            fighter.health = 100;
        }
        if(fighter.power < 1 || fighter.power > 100) {
            res.body = res.status(400).json({
                error: true,
                message: 'Power can\'t be less than 1 or more than 100'
            });
            next(res.body);
        }
        if(fighter.defense < 1 || fighter.defense > 10) {
            res.body = res.status(400).json({
                error: true,
                message: 'Defense can\'t be less than 1 or more than 10'
            })
            next(res.body);
        }
    }else{
        res.body = res.status(400).json({
            error: true,
            message: 'Fighter entity to create isn\'t valid'
        });
        next(res.body);
    }


    // TODO: Implement validatior for fighter entity during creation
    next();
}

const updateFighterValid = (req, res, next) => {
    fighter.defense = req.body.defense;
    fighter.name = req.body.name;
    fighter.power = req.body.power;
    const shouldContainOnly = ['name','power','defense'];

    for(let [key] of Object.entries(req.body)){
        if(!shouldContainOnly.includes(key)) {
            res.body = res.status(400).json({
                error: true,
                message: `Request contains unknown property ${key}`
            });
            next(res.body);
        }
    }

    if(fighter.name || fighter.power || fighter.defense) {
        if(req.body.id) {
            res.body = res.status(400).json({
                error: true,
                message: 'Id shouldn\'t be provided in body'
            });
            next(res.body);
        }
        if(req.body.health && req.body.health <= 120 && req.body.health >= 80) {
            fighter.health = req.body.health;
        }else {
            fighter.health = 100;
        }
        if(fighter.power && fighter.power < 1 || fighter.power > 100) {
            res.body = res.status(400).json({
                error: true,
                message: 'Power can\'t be less than 1 or more than 100'
            });
            next(res.body);
        }
        if(fighter.defense && fighter.defense < 1 || fighter.defense > 10) {
            res.body = res.status(400).json({
                error: true,
                message: 'Defense can\'t be less than 1 or more than 10'
            });
            next(res.body)
        }
    }else{
        res.body = res.status(400).json({
            error: true,
            message: 'Fighter entity to create isn\'t valid'
        });
        next(res.body);
    }
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;